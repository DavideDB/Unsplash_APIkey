var axios = require('axios');
var wget = require('node-wget');

var endpointDomain = 'https://api.unsplash.com/';
var request = 'photos/random/';
var client_id = '073fdb56136c670892b53ff003b5f7be144202d25768fc1e587ca8d1e55c6e4f';
var count = 5;

var requestUrl = endpointDomain + request;

axios.get(requestUrl, {
        params: {
            client_id: client_id,
            count: count
        }
      })
      .then(function (response) {
            for(var i = 0; i < count; i++){
                wget({
                    url: response.data[i].urls.raw,
                    dest: 'image_'+ i + '.jpg'
                },
                    function (error) {
                        if (error) {
                            console.log('--- error:');
                        } else {
                            console.log('Image downloaded');
                        }
                    }
                );
            }
          }
      )
      .catch(function (error) {
        console.log(error);
      });



    